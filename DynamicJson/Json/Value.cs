﻿using System;
using System.Collections.Generic;

namespace DynamicJson.Json
{
    public abstract class Value
    {
        public static Value MakeObject(Dictionary<string, Value> values)
        {
            return new Object(values);
        }

        public static Value MakeArray(IEnumerable<Value> values)
        {
            return new Array(values);
        }

        public static Value MakeNumber(decimal value)
        {
            return new Number(value);
        }

        public static Value MakeString(string value)
        {
            return new String(value);
        }

        public static Value MakeBoolean(bool value)
        {
            return new Boolean(value);
        }

        public static Value MakeNull()
        {
            return new Null();
        }
    }

    public class Object : Value
    {
        public Dictionary<string, Value> Value { get; private set; }

        public Object()
        {
            Value = new Dictionary<string, Value>();
        }
        public Object(Dictionary<string, Value> values)
        {
            Value = new Dictionary<string, Value>(values);
        }
        public Object(string key, Value value)
        {
            Value = new Dictionary<string, Value>();
            Value[key] = value;
        }

        public void Insert(Object value)
        {
            foreach (var v in value.Value)
            {
                Value.Add(v.Key, v.Value);
            }
        }

        public void Insert(string key, Value value)
        {
        }

        public Value this[string key]
        {
            get
            {
                return Value[key];
            }
            set
            {
                Value[key] = value;
            }
        }

        public Dictionary<string, Value>.Enumerator GetEnumerator()
        {
            return Value.GetEnumerator();
        }
    }

    public class String : Value
    {
        public string Value { get; set; }

        public static implicit operator string(String s)
        {
            return s.Value;
        }

        public String(string value)
        {
            Value = value;
        }
    }

    public class Array : Value
    {
        public List<Value> Value { get; private set; }

        public Array()
        {
            Value = new List<Value>();
        }

        public Array(IEnumerable<Value> values)
        {
            Value = new List<Value>(values);
        }

        public void Add(Value value)
        {
            Value.Add(value);
        }

        public IEnumerator<Value> GetEnumerator()
        {
            return Value.GetEnumerator();
        }
    }

    public class Number : Value
    {
        public decimal Value { get; set; }

        public static implicit operator decimal(Number n)
        {
            return n.Value;
        }

        public Number(decimal value)
        {
            Value = value;
        }
    }

    public class Boolean : Value
    {
        public bool Value { get; set; }

        public static implicit operator bool(Boolean b)
        {
            return b.Value;
        }

        public Boolean(bool value)
        {
            Value = value;
        }
    }

    public class Null : Value
    {
        public object Value { get { return null; } }
    }

    public class InvalidValue : Value
    {
        public object Value { get { throw new Exception(); } }
    }
}