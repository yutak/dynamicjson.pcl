﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DynamicJson.Json
{
    internal class LexerHandle
    {
        private readonly LexerBase _base;

        public LexerHandle(TextReader reader)
        {
            _base = new LexerIterator(reader);
        }

        public bool IsEnd()
        {
            return _base.IsEnd();
        }

        public char Peek()
        {
            return _base.Peek();
        }

        public void Next()
        {
            _base.Next();
        }

        private abstract class LexerBase
        {
            public abstract bool IsEnd();
            public abstract char Peek();
            public abstract void Next();
        }

        private class LexerIterator : LexerBase
        {
            private readonly TextReader _reader;

            public LexerIterator(TextReader reader)
            {
                _reader = reader;
            }

            public override bool IsEnd()
            {
                return _reader.Peek() < 0;
            }

            public override char Peek()
            {
                return (char)_reader.Peek();
            }

            public override void Next()
            {
                _reader.Read();
            }
        };
    }

    public class Lexer
    {
        private readonly LexerHandle _handle;

        public Lexer(TextReader reader, string filename = "", int lineno = 1)
        {
            _handle = new LexerHandle(reader);
            FileName = filename;
            LineNo = lineno;
        }

        public string FileName { get; set; }
        public int LineNo { get; set; }

        protected void Skip()
        {
            while (char.IsWhiteSpace(PeekAnyChar())) LexAnyChar();
        }

        protected char PeekAnyChar()
        {
            return _handle.IsEnd() ? (char)0 : _handle.Peek();
        }

        protected char LexAnyChar()
        {
            var c = PeekAnyChar();
            if (c == '\n') ++LineNo;
            if (c != 0) _handle.Next();
            return c;
        }

        protected char LexOneChar(params char[] p)
        {
            return p.FirstOrDefault(LexChar);
        }

        protected char PeekOneChar(params char[] p)
        {
            return p.FirstOrDefault(PeekChar);
        }

        protected char LexOneChar(string p)
        {
            return LexOneChar(p.ToCharArray());
        }

        protected char PeekOneChar(string p)
        {
            return PeekOneChar(p.ToCharArray());
        }

        protected bool PeekChar(char c)
        {
            return PeekAnyChar() == c;
        }

        protected bool LexChar(char c)
        {
            if (PeekChar(c))
            {
                LexAnyChar();
                return true;
            }
            return false;
        }

        protected bool LexHexDigit(out uint val)
        {
            val = 0;
            char c = char.ToUpper(PeekAnyChar());
            if (!IsHexDigit(c)) return false;
            LexAnyChar();
            if (char.IsDigit(c)) val = (uint)(c - '0');
            else val = (uint)((c - 'A') + 10);
            return true;
        }

        protected bool IsHexDigit(char c)
        {
            return "0123456789ABCDEF".IndexOf(c) >= 0;
        }
    }

    public class ParseError
    {
        public string Message { get; set; }
        public string FileName { get; set; }
        public int LineNo { get; set; }
    }

    public struct ParseResult
    {
        public Value Result { get; internal set; }
        public List<ParseError> Errors { get; internal set; }
    };

    public class Parser : Lexer
    {
        private readonly List<ParseError> _errors;

        public Parser(TextReader reader, string filename = "", int lineno = 1)
            : base(reader, filename, lineno)
        {
            _errors = new List<ParseError>();
        }

        public ParseResult Parse()
        {
            var result = new ParseResult();
            result.Result = ParseValue();
            result.Errors = _errors;
            return result;
        }

        private Object ParsePair()
        {
            Skip();
            string str;
            if (PeekChar('"'))
                str = ParseString();
            else
            {
                str = ParseSymbol();
                HandleError(string.IsNullOrWhiteSpace(str)
                    ? "要素名は文字列でなければなりません。"
                    : "要素名(" + str + ")は文字列でなければなりません。");
            }
            Skip();
            if (!LexChar(':'))
            {
                if (PeekChar(','))
                {
                    HandleError("objectの要素の値がありません");
                    return new Object(str, new InvalidValue());
                }
                HandleError("objectの要素の間に:がありません。");
            }
            var obj = ParseValue();
            return new Object(str, obj);
        }

        private char ParseChar()
        {
            if (PeekChar('"'))
            {
                HandleError("文字の中に'\"'が出現しました。");
            }
            if (!LexChar('\\'))
            {
                return LexAnyChar();
            }
            var ch = LexAnyChar();
            switch (ch)
            {
                case (char)0:
                    HandleError("解析できる文字がありません");
                    return '\0';
                case '"':
                    return '"';
                case '\\':
                    return '\\';
                case '/':
                    return '/';
                case 'b':
                    return '\b';
                case 'f':
                    return '\f';
                case 'n':
                    return '\n';
                case 'r':
                    return '\r';
                case 't':
                    return '\t';
                case 'u':
                    {
                        uint a, b, c, d;
                        if (
                            LexHexDigit(out a) &&
                            LexHexDigit(out b) &&
                            LexHexDigit(out c) &&
                            LexHexDigit(out d)
                            )
                            return (char)(a << 12 | b << 8 | c << 4 | d);
                        HandleError("\\uXXXXはXは4文字で16進数文字でなければなりません。");
                        return '.';
                    }
                default:
                    HandleError("'" + ch + "'はエスケープシーケンスとして定義されていません。");
                    return '.';
            }
        }

        private string ParseSymbol()
        {
            var result = "";
            while (char.IsLetterOrDigit(PeekAnyChar()))
            {
                result += LexAnyChar();
            }
            return result;
        }

        private string ParseString()
        {
            if (!LexChar('"')) HandleError("Stringは\"から始まる必要があります。");
            var result = "";
            for (; ; )
            {
                if (PeekAnyChar() == (char)0)
                {
                    HandleError("Stringの終端の\"が見つかりませんでした。");
                    return result;
                }
                if (LexChar('"')) return result;
                result += ParseChar();
            }
        }

        private string ParseDigits()
        {
            var result = "";
            char ch;
            while ((ch = LexOneChar("0123456789")) != (char)0)
                result += ch;
            return result;
        }

        private decimal ParseNumber()
        {
            var sign = LexChar('-');
            var ch = LexOneChar("L0123456789");
            if (ch == (char)0)
            {
                //1文字も数字がないだと！
                HandleError(sign ? "'-'の後ろには数値列が見つかりません。" : "Numberとして解釈しましたが数値が見つかりませんでした。");
                return 0m;
            }
            var intpart = "";
            intpart += ch;
            if (ch == '0')
            {
                if (LexOneChar("xX") != 0)
                {
                    HandleError("16進数はJSONでサポートされていません。");
                }
                else if (PeekOneChar("0123456789") != 0)
                {
                    HandleError("8進数はJSONでサポートされていません。");
                }
            }
            intpart += ParseDigits();
            var fracpart = "";
            if (LexChar('.'))
            {
                fracpart = "." + ParseDigits();
                if (fracpart.Length == 1) HandleError(".の後には最低でも数字が1つは必要です");
            }
            var exppart = 1m;
            if (LexOneChar("eE") != 0)
            {
                var q = LexOneChar("-+");
                if (q != 0) exppart = q == '-' ? -1 : 1;
                var digits = ParseDigits();
                if (!string.IsNullOrEmpty(digits))
                {
                    var d = double.Parse(digits);
                    var exp = (UInt64)Math.Pow(10, d);
                    if (exppart < 0) exppart = 1m / exp;
                    else exppart = exp;
                }
                else HandleError("eの後の指数には少なくとも数字が1つは必要です");
            }
            var number = intpart + fracpart;
            return Convert.ToDecimal(number) * (sign ? -1 : 1) * exppart;
        }

        private Object ParseObject()
        {
            if (!LexChar('{')) HandleError("objectは'{'から始まらなければなりません。");
            Skip();
            var result = new Object();
            if (LexChar('}')) return result;
            result.Insert(ParsePair());
            for (; ; )
            {
                Skip();
                if (PeekAnyChar() == 0)
                {
                    HandleError("objectの終端の'}'が見つかりませんでした。");
                    return result;
                }
                if (LexChar('}')) return result;
                if (LexChar(','))
                {
                    Skip();
                    if (LexChar('}'))
                    {
                        HandleError("objectの最後の要素の後ろに','は正しくありません。");
                        return result;
                    }
                }
                else
                    HandleError("objectの要素は','で区切られている必要があります。");
                result.Insert(ParsePair());
            }
        }

        private Array ParseArray()
        {
            if (!LexChar('[')) HandleError("arrayは'['から始まる必要があります。");
            Skip();
            var result = new Array();
            if (LexChar(']')) return result;
            result.Add(ParseValue());
            for (; ; )
            {
                Skip();
                if (PeekAnyChar() == 0)
                {
                    HandleError("arrayの終端の']'が見つかりませんでした。");
                    return result;
                }
                if (LexChar(']')) return result;
                if (LexChar(','))
                {
                    Skip();
                    if (LexChar(']'))
                    {
                        HandleError("arrayの末尾の要素の後ろに','は正しくありません。");
                        return result;
                    }
                }
                else
                    HandleError("arrayの要素は','で区切られている必要があります。");
                result.Add(ParseValue());
            }
        }

        private Value ParseValue()
        {
            Skip();
            if (PeekChar('"')) return new String(ParseString());
            else if (PeekOneChar("0123456789-") != 0) return new Number(ParseNumber());
            else if (PeekChar('{')) return ParseObject();
            else if (PeekChar('[')) return ParseArray();

            var symbol = ParseSymbol();

            if (string.IsNullOrEmpty(symbol))
            {
                HandleError("値として解析できませんでした。");
                LexAnyChar(); //一文字も進んでないと無限ループになるため
                return new InvalidValue();
            }
            if (symbol == "true") return new Boolean(true);
            if (symbol == "false") return new Boolean(false);
            if (symbol == "null") return new Null();
            HandleError("'" + symbol + "' : 定義されていない識別子です。");
            return new String(symbol);
        }

        private void HandleError(string msg)
        {
            _errors.Add(new ParseError { Message = msg, FileName = FileName, LineNo = LineNo });
        }
    }
}