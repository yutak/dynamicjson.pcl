﻿using System.IO;
using System.Text;

namespace DynamicJson.Json
{
    static public class XmlConverter
    {
        private static string QuoteXmlEntities(string str)
        {
            // TODO: should be esape xml entities
            return str;
        }

        private static string ToXml(string name, Value value)
        {
            if (value is Object)
            {
                var sb = new StringBuilder(string.Format(@"<{0} type=""object"">", name));
                foreach (var v in (Object)value)
                {
                    sb.Append(ToXml(v.Key, v.Value));
                }
                sb.Append(string.Format("</{0}>", name));
                return sb.ToString();
            }
            else if (value is String)
            {
                return string.Format(@"<{0} type=""string"">{1}</{0}>", name, QuoteXmlEntities((String)value));
            }
            else if (value is Array)
            {
                var sb = new StringBuilder(string.Format(@"<{0} type=""array"">", name));
                foreach (var v in (Array)value)
                {
                    sb.Append(ToXml("item", v));
                }
                sb.Append(string.Format("</{0}>", name));
                return sb.ToString();
            }
            else if (value is Number)
            {
                return string.Format(@"<{0} type=""number"">{1}</{0}>", name, ((Number)value).Value);
            }
            else if (value is Boolean)
            {
                return string.Format(@"<{0} type=""boolean"">{1}</{0}>", name, (Boolean) value ? "true" : "false");
            }
            else if (value is Null)
            {
                return string.Format(@"<{0} type=""null"" />", name);
            }
            return "";
        }

        public static TextReader ToXml(this Value value)
        {
            return new StringReader(ToXml("root", value));
        }
    }

    public static class JsonConverter
    {
        public static string ToJson(this Value value)
        {
            if (value is Object)
            {
                var sb = new StringBuilder("{");
                bool f = false;
                foreach (var v in (Object)value)
                {
                    if (f) sb.Append(",");
                    sb.Append(string.Format(@"""{0}"":{1}", v.Key, v.Value.ToJson()));
                    f = true;
                }
                sb.Append("}");
                return sb.ToString();
            }
            else if (value is String)
            {
                return string.Format(@"""{0}""", ((String)value).Value);
            }
            else if (value is Array)
            {
                var sb = new StringBuilder("[");
                var f = false;
                foreach (var v in (Array)value)
                {
                    if (f) sb.Append(",");
                    sb.Append(v.ToJson());
                    f = true;
                }
                sb.Append("]");
                return sb.ToString();
            }
            else if (value is Number)
            {
                return string.Format(@"{0}", ((Number)value).Value);
            }
            else if (value is Boolean)
            {
                return (Boolean)value ? "true" : "false";
            }
            else if (value is Null)
            {
                return "null";
            }
            return "";
        }
    }
}
